﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;
using System.Web.Routing;
using StructureMap;

namespace Aviva.FizzBuzz
{
    /// <summary>
    /// Structuremap controller factory
    /// </summary>
    public class StructureMapCF : DefaultControllerFactory
    {
        /// <summary>
        /// Gets the controller instance
        /// </summary>
        /// <param name="requestContext">request context</param>
        /// <param name="controllerType">controller type</param>
        /// <returns>Controller</returns>
        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            if (controllerType == null)
                return base.GetControllerInstance(requestContext, controllerType);

            return ObjectFactory.GetInstance(controllerType) as Controller;
        }
    }
}