﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Reflection;

namespace Aviva.FizzBuzz.Common
{
    public class ButtonAttribute : ActionMethodSelectorAttribute
    {
        public string ButtonName { get; set; }
        public override bool IsValidForRequest(ControllerContext controllerContext, MethodInfo methodInfo)
        {
            return controllerContext.Controller.ValueProvider.GetValue(ButtonName) != null;
        }
    }
}