﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using FizzBuzz.Model;
using Aviva.FizzBuzz.Common;
using Aviva.FizzBuzz.Models;
using System.Collections;
using StructureMap;

namespace Aviva.FizzBuzz.Controllers
{
    public class FizzBuzzController : Controller
    {
        private readonly IStrategic strategicLogic;
        private readonly IList<IFizzBuzz> fizzbuzz;

        /// <summary>
        /// Controller. Creates an instance of FizzBuzzController
        /// </summary>
        public FizzBuzzController(IStrategic StrategicLogic, IList<IFizzBuzz> FizzBuzz)
        {
            this.strategicLogic     = StrategicLogic;
            this.fizzbuzz           = FizzBuzz;
        }

        /// <summary>
        /// Action method to render the initial view.
        /// </summary>
        /// <returns>FizzBuzzView View</returns>
        [HttpGet]
        public ActionResult FizzBuzzView()
        {           
            return View("FizzBuzzView", new FizzBuzzModel());
        }

        /// <summary>
        /// Action method to render on Get Data button submit.
        /// </summary>
        /// <returns>FizzBuzzView View</returns>
        [HttpPost]
        [Button(ButtonName = "DisplayList")]
        [ActionName("FizzBuzzView")]
        public ActionResult FizzBuzzView(FizzBuzzModel PostModel)
        {
            FizzBuzzModel objData = new FizzBuzzModel() { Number = PostModel.Number };
            objData.Page.PageIndex = 1;

            if (ModelState.IsValid) { fillFizzBuzzList(objData); }
            TempData["FizzBuzzPage"] = objData.Page;
            return View("FizzBuzzView", objData);
        }

                /// <summary>
        /// Action method to render on Previous button click.
        /// </summary>
        /// <returns>FizzBuzzView View</returns>
        [HttpPost]
        [Button(ButtonName = "Previous")]
        [ActionName("FizzBuzzView")]
        public ActionResult FizzBuzzViewPrevious(FizzBuzzModel PostModel)
        {
            FizzBuzzModel objData   = new FizzBuzzModel() { Number = PostModel.Number };
            objData.Page            = (TempData["FizzBuzzPage"] != null) ? TempData["FizzBuzzPage"] as Pagging : new Pagging();
            objData.Page.PageIndex  = (objData.Page.PageIndex == 1) ? objData.Page.PageIndex : (objData.Page.PageIndex - 1);

            if (ModelState.IsValid) { fillFizzBuzzList(objData); }
            TempData["FizzBuzzPage"] = objData.Page;
            return View("FizzBuzzView", objData);
        }

        /// <summary>
        /// Action method to render on Next button click.
        /// </summary>
        /// <returns>FizzBuzzView View</returns>
        [HttpPost]
        [Button(ButtonName = "Next")]
        [ActionName("FizzBuzzView")]
        public ActionResult FizzBuzzViewNext(FizzBuzzModel PostModel)
        {
            FizzBuzzModel objData   = new FizzBuzzModel() { Number = PostModel.Number };
            objData.Page            = (TempData["FizzBuzzPage"] != null) ? TempData["FizzBuzzPage"] as Pagging : new Pagging();
            objData.Page.PageIndex  = (objData.Page.PageIndex == objData.Page.PageCount) ? objData.Page.PageIndex : (objData.Page.PageIndex + 1);

            if (ModelState.IsValid) { fillFizzBuzzList(objData); }
            TempData["FizzBuzzPage"] = objData.Page;
            return View("FizzBuzzView", objData);
        }

        private void fillFizzBuzzList(FizzBuzzModel objData)
        {
            objData.FizzBuzzList    = strategicLogic.GetList(objData.Number.Value, fizzbuzz);
            var startndex           = (objData.Page.PageIndex - 1) * FizzBuzzModel.PageSize;
            objData.FizzBuzzList    = objData.FizzBuzzList.GetRange(startndex, objData.Number.Value - startndex < FizzBuzzModel.PageSize ? objData.Number.Value - startndex : FizzBuzzModel.PageSize);
        }

    }
}
