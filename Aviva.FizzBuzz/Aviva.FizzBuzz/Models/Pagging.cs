﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aviva.FizzBuzz.Models
{
    /// <summary>
    /// Pagging for have current pageIndex and PageCount
    /// </summary>
    public class Pagging
    {
        /// <summary>
        /// Set Default value in constructure
        /// </summary>
        public Pagging(){ 
            PageIndex = 1; 
        }

        /// <summary>
        /// Page index of the pagination.
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// Page count of the pagination.
        /// </summary>
        public int PageCount { get; set; }
    }
}