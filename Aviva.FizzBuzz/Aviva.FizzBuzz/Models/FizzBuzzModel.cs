﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using FizzBuzz.Model;

namespace Aviva.FizzBuzz.Models
{
     /// <summary>
    /// The view model for the Fizz Buzz Operation
    /// </summary>
    public class FizzBuzzModel
    {
        /// <summary>
        /// Default value of Page Size
        /// </summary>
        public const int PageSize = 20;

        public FizzBuzzModel()
        {
            Page = new Pagging();
        }

        /// <summary>
        /// Get or set Page to view pagging on ViewPage
        /// </summary>
        public Pagging Page { get; set; }

        int? nNumber;
        /// <summary>
        /// Gets or sets the input number.
        /// </summary>
        [Required]
        [Display(Name = "Please enter the number")]
        [Range(1, 1000, ErrorMessage = "Please enter a value between 1 and 1000")]
        public int? Number {
            get
            {
                return nNumber;
            }
            set
            {
                nNumber         = value;
                Page.PageIndex  = 1;
                Page.PageCount  = (Convert.ToInt32(nNumber) / PageSize);
                Page.PageCount  = ((nNumber % PageSize)) == 0 ? Page.PageCount : Page.PageCount + 1;
            }
        }

        /// <summary>
        /// Gets or sets the Division object list.
        /// </summary>
        public List<IFizzBuzz> FizzBuzzList { get; set; }        
    }
}