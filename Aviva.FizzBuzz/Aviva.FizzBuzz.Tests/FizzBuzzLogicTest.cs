﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using StructureMap;
using FizzBuzz.Model;

namespace Aviva.FizzBuzz.Tests
{
    [TestFixture]
    public class FizzBuzzLogicTest
    {
        [SetUp]
        public static void Initialize_FizzBuzzStructuremap()
        {
            ObjectFactory.Initialize(m =>
            {
                //Scan all class those inherit with IFizzBuzz
                m.Scan(x =>
                {
                    x.Assembly("FizzBuzz.Model");
                    x.AddAllTypesOf<IFizzBuzz>();
                    x.WithDefaultConventions();
                });

                //Set all the instance with IFizzBuzz
                m.For<IList<IFizzBuzz>>().Use(x => x.GetAllInstances<IFizzBuzz>().ToList());
            });
        }

        [Test]
        public void Input_Number_24_Should_Have_24_NumbersCount()
        {
            IList<IFizzBuzz> listInstance = ObjectFactory.GetInstance<List<IFizzBuzz>>();
            IStrategic StrategicLogic = new FizzBuzzStrategic();
            List<IFizzBuzz> list = StrategicLogic.GetList(24, listInstance);
            Assert.AreEqual(24, list.Count);
            Assert.AreEqual("1", ((IFizzBuzz)list[0]).Message);
            Assert.AreEqual("2", ((IFizzBuzz)list[1]).Message);
        }

        [Test]
        public void Input_Number_Replace_3_Divisible_With_Proper_Message_Color()
        {
            IList<IFizzBuzz> listInstance = ObjectFactory.GetInstance<List<IFizzBuzz>>();
            IStrategic StrategicLogic = new FizzBuzzStrategic();
            List<IFizzBuzz> list = StrategicLogic.GetList(24, listInstance);
            if (DateTime.Today.DayOfWeek == DayOfWeek.Wednesday)
            {
                Assert.AreEqual("Wizz", ((IFizzBuzz)list[2]).Message);
                Assert.AreEqual("Wizz", ((IFizzBuzz)list[5]).Message);
            }
            else
            {
                Assert.AreEqual("Fizz", ((IFizzBuzz)list[2]).Message);
                Assert.AreEqual("Fizz", ((IFizzBuzz)list[5]).Message);
            }
            Assert.AreEqual("blue", ((IFizzBuzz)list[2]).MessageColor);
            Assert.AreEqual("blue", ((IFizzBuzz)list[5]).MessageColor);
        }

        [Test]
        public void Input_Number_Replace_5_Divisible_With_Proper_Message_Color()
        {
            IList<IFizzBuzz> listInstance = ObjectFactory.GetInstance<List<IFizzBuzz>>();
            IStrategic StrategicLogic = new FizzBuzzStrategic();
            List<IFizzBuzz> list = StrategicLogic.GetList(24, listInstance);
            if (DateTime.Today.DayOfWeek == DayOfWeek.Wednesday)
            {
                Assert.AreEqual("Wuzz", ((IFizzBuzz)list[4]).Message);
                Assert.AreEqual("Wuzz", ((IFizzBuzz)list[9]).Message);
            }
            else
            {
                Assert.AreEqual("Buzz", ((IFizzBuzz)list[4]).Message);
                Assert.AreEqual("Buzz", ((IFizzBuzz)list[9]).Message);
            }
            Assert.AreEqual("green", ((IFizzBuzz)list[4]).MessageColor);
            Assert.AreEqual("green", ((IFizzBuzz)list[9]).MessageColor);
        }

        [Test]
        public void Input_Number_Replace_3_and_5_Divisible_With_Proper_Message()
        {
            IList<IFizzBuzz> listInstance = ObjectFactory.GetInstance<List<IFizzBuzz>>();
            IStrategic StrategicLogic = new FizzBuzzStrategic();
            List<IFizzBuzz> list = StrategicLogic.GetList(24, listInstance);
            if (DateTime.Today.DayOfWeek == DayOfWeek.Wednesday)
            {
                Assert.AreEqual("Wizz Wuzz", ((IFizzBuzz)list[14]).Message);
            }
            else
            {
                Assert.AreEqual("Fizz Buzz", ((IFizzBuzz)list[14]).Message);
            }
        }
    }
}
