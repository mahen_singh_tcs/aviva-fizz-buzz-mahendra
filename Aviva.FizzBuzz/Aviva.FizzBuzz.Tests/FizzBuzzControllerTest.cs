﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Moq;
using NUnit.Framework;
using StructureMap;
using FizzBuzz.Model;
using Aviva.FizzBuzz.Controllers;
using Aviva.FizzBuzz.Models;
using System.Web.Mvc;

namespace Aviva.FizzBuzz.Tests
{
    [TestFixture]
    public class FizzBuzzControllerTest
    {
        [SetUp]
        public static void Initialize_FizzBuzzStructuremap()
        {
            ObjectFactory.Initialize(m =>
            {
                //Scan all class those inherit with IFizzBuzz
                m.Scan(x =>
                {
                    x.Assembly("FizzBuzz.Model");
                    x.AddAllTypesOf<IFizzBuzz>();
                    x.WithDefaultConventions();
                });

                //Set all the instance with IFizzBuzz
                m.For<IList<IFizzBuzz>>().Use(x => x.GetAllInstances<IFizzBuzz>().ToList());
            });
        }

        [Test]
        public void Should_Give_FizzBuzzView_On_InputRequest()
        {
            IList<IFizzBuzz> listInstance       = ObjectFactory.GetInstance<List<IFizzBuzz>>();
            Mock<IStrategic> StrategicLogic     = new Mock<IStrategic>();
            FizzBuzzController controllerToTest = new FizzBuzzController(StrategicLogic.Object, listInstance);
            var resultView                      = controllerToTest.FizzBuzzView() as ViewResult;
            Assert.AreEqual("FizzBuzzView", resultView.ViewName);
        }
        
        [Test]
        public void Should_Show__20_Items_On_One_Page()
        {
            IList<IFizzBuzz> listInstance   = ObjectFactory.GetInstance<List<IFizzBuzz>>();
            Mock<IStrategic> StrategicLogic = new Mock<IStrategic>();               
            StrategicLogic.Setup(m => m.GetList(21,listInstance)).Returns(getFizzBuzzList(21));
            FizzBuzzController controller   = new FizzBuzzController(StrategicLogic.Object, listInstance);
            var result                      = controller.FizzBuzzView(new FizzBuzzModel { Number = 21 }) as ViewResult;
            var viewModel                   = (FizzBuzzModel)result.Model;
            Assert.AreEqual(20, viewModel.FizzBuzzList.Count);
        }

        [Test]
        public void Should_implement_PageCount()
        {
            IList<IFizzBuzz> listInstance   = ObjectFactory.GetInstance<List<IFizzBuzz>>();
            Mock<IStrategic> StrategicLogic = new Mock<IStrategic>();            
            StrategicLogic.Setup(m => m.GetList(63, listInstance)).Returns(getFizzBuzzList(63));
            FizzBuzzController controller   = new FizzBuzzController(StrategicLogic.Object, listInstance);
            var result                      = controller.FizzBuzzView(new FizzBuzzModel { Number = 63 }) as ViewResult;
            var viewModel                   = (FizzBuzzModel)result.Model;
            Assert.AreEqual(4, viewModel.Page.PageCount);
        }

        [Test]
        public void Should_implement_paging()
        {
            IList<IFizzBuzz> listInstance   = ObjectFactory.GetInstance<List<IFizzBuzz>>();
            Mock<IStrategic> StrategicLogic = new Mock<IStrategic>();            
            StrategicLogic.Setup(m => m.GetList(21, listInstance)).Returns(getFizzBuzzList(21));
            FizzBuzzController controller = new FizzBuzzController(StrategicLogic.Object, listInstance);
            var result                      = controller.FizzBuzzViewNext(new FizzBuzzModel { Page = new Pagging() { PageIndex = 2 }, Number = 21 }) as ViewResult;
            var viewModel                   = (FizzBuzzModel)result.Model;
            Assert.AreEqual(1, viewModel.FizzBuzzList.Count); //Second page shouold have ony 1 Value
        }

        private List<IFizzBuzz> getFizzBuzzList(int rows)
        {
            List<IFizzBuzz> list = new List<IFizzBuzz>();
            for (int index = 1; index <= rows; index++){                
                list.Add(new Number(index));
            }
            return list;
        }
    }
}
