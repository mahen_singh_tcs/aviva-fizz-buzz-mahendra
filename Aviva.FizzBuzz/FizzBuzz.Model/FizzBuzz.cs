﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Model
{
    /// <summary>
    /// FizzBuzz Class
    /// </summary>
    public class FizzBuzz : IFizzBuzz
    {
        /// <summary>
        /// Number Divisible by 3 and 5
        /// </summary>
        /// <param name="Number"></param>
        /// <returns>boolean value</returns>
        public bool IsDivisible(int Number){
            return ((Number % 3) == 0 && (Number % 5) == 0) ? true : false;
        }

        string sMessage;
        public string Message
        {
            get { return sMessage; }
        }

        public string MessageColor
        {
            get { return string.Empty; }
        }

        /// <summary>
        /// Set message on SpecificDay value
        /// </summary>
        /// <param name="IsSpecificDay"></param>
        public void SetMessage(bool IsSpecificDay)
        {
            sMessage = IsSpecificDay ? "Wizz Wuzz" : "Fizz Buzz";
        }
    }

    /// <summary>
    /// Fizz class
    /// </summary>
    public class Fizz : IFizzBuzz
    {
        /// <summary>
        /// Number Divisible by 3
        /// </summary>
        /// <param name="Number"></param>
        /// <returns>boolean value</returns>
        public bool IsDivisible(int Number)
        {
            return (Number % 3) == 0 ? true : false;
        }

        string sMessage;
        public string Message
        {
            get { return sMessage; }
        }

        public string MessageColor
        {
            get { return "blue"; }
        }

        public void SetMessage(bool IsSpecificDay)
        {
            sMessage = IsSpecificDay ? "Wizz" : "Fizz";
        }
    }

    /// <summary>
    /// Buzz class
    /// </summary>
    public class Buzz : IFizzBuzz
    {
        /// <summary>
        /// Number Divisible by 5
        /// </summary>
        /// <param name="Number"></param>
        /// <returns>boolean value</returns>
        public bool IsDivisible(int Number)
        {
            return (Number % 5) == 0 ? true : false;
        }

        string sMessage;
        public string Message
        {
            get { return sMessage; }
        }

        public string MessageColor
        {
            get { return "green"; }
        }

        public void SetMessage(bool IsSpecificDay)
        {
            sMessage = IsSpecificDay ? "Wuzz" : "Buzz";
        }
    }    
}
