﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Model
{
    /// <summary>
    /// Abstraction of Strategic logic
    /// </summary>
    public interface IStrategic
    {
        /// <summary>
        /// Gets the list of numbers from one upto the number given.
        /// </summary>
        /// <param name="number">The input number.</param>
        /// <returns>List of IDivision</returns>
        List<IFizzBuzz> GetList(int nNumber, IList<IFizzBuzz> listFizzBuzz);

        bool IsSpecificDay();
    }
}
