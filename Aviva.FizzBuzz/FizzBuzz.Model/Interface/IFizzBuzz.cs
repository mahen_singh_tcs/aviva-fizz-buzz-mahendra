﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Model
{
    /// <summary>
    /// Abstraction of FizzBuzz
    /// </summary>
    public interface IFizzBuzz
    {
        bool IsDivisible(int Number);
        string Message { get; }
        string MessageColor { get; }
        void SetMessage(bool IsSpecificDay);
    }
}
