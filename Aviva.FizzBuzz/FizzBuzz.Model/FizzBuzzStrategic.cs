﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Model
{
     /// <summary>
    /// The business logic of the application
    /// </summary>
    public class FizzBuzzStrategic : IStrategic
    {
        /// <summary>
        /// Gets the list of numbers from one upto the number given.
        /// </summary>
        /// <param name="number">The input number.</param>
        /// <returns>List of numbers</returns>
        public List<IFizzBuzz> GetList(int number, IList<IFizzBuzz> listFizzBuzz)
        {
            List<IFizzBuzz> list = new List<IFizzBuzz>();
            for (var counter = 1; counter <= number; counter++)
            {
                //Get the class instance if num is divisiable
                IEnumerable<IFizzBuzz> data = listFizzBuzz.Where(x => x.IsDivisible(counter) == true);
                if(data != null && data.ToList().Count > 0){
                    IFizzBuzz objData = data.ToList()[0] as IFizzBuzz;
                    objData.SetMessage(IsSpecificDay());
                    list.Add(objData);                                                                          
                }
                else{
                    list.Add(new Number(counter)); 
                }
            }

            return list;
        }

        /// <summary>
        /// Function check for specific day
        /// </summary>
        /// <returns>return boolean value</returns>
        public bool IsSpecificDay()
        {
            return DateTime.Today.DayOfWeek == DayOfWeek.Wednesday ? true : false;
        }
    } 
}
